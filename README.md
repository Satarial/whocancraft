# WhoCanCraft

## Notes

- only tested with 1.12 client (on Lightbringer)
- this is work in progress and is bound to contain bugs


## Usage 

1. Copy files to `<wow folder>\Interface\AddOns\WhoCanCraft\`
2. Enable the addon in-game
3. To scan the recipes you can craft, open up each profession tab at least once (e.g. Tailoring)
4. The addon *listens* for guild messages starting with `wcc` (e.g. `wcc linen`) and will whisper the respective player with a list of available recipes which contain the query 	
  e.g. `wcc linen` will prompt a reply from guild members that have the addon installed and and can craft the recipes that match the query - ```"I can craft: [Bolt of Linen Cloth], [White Linen Robe], ..."```)