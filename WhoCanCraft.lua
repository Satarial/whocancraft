local function PrintMsg(message)
	DEFAULT_CHAT_FRAME:AddMessage(message or "error");
end


local function LoadDefaults(currentVersion)
	WCC = {
        version = currentVersion,
        db = {},
	};
	PrintMsg("WhoCanCraft: Default Settings Loaded.");
end


local function LoadVariables()
	local version = GetAddOnMetadata("WhoCanCraft", "Version");
	
	if not(WCC and WCC.version and WCC.db) then
		LoadDefaults(version);
	elseif WCC.version ~= version then
		WCC.version = version;
		PrintMsg("WhoCanCraft: Updated to v" .. WCC.version);
	end
end


professions = {
    "Tailoring",
    "Enchanting",
    "Blacksmithing",
    "Leatherworking",
    "Alchemy",
    "Engineering",
    "Cooking",
}


SLASH_WCC1 = "/wcc";
-- used for debugging
function SlashCmdList.WCC(query)
    canCraft = canICraft(query);
    if table.getn(canCraft) > 0 then
        PrintMsg("I can craft: " .. table.concat(canCraft, ", "));
    end
end


function canICraft(query)
    local results = {}

    for _, p in pairs(WCC.db) do
        for _, v in pairs(p) do
            if string.find(string.lower(v["name"]), string.lower(query)) then
                table.insert(results, v["link"])
            end
        end
    end

    return results
end


function getTradeSkills()
    local name, type, link
    local tradeskillName, _, _ = GetTradeSkillLine()
    if not(table.contains(professions, tradeskillName)) then
        return false
    end
    WCC.db[tradeskillName] = {}
    for i = 1, GetNumTradeSkills() do
        name, type, _, _, _, _ = GetTradeSkillInfo(i)
        link = GetTradeSkillItemLink(i)
        if (name and link and tradeskillName and type ~= "header") then
            table.insert(WCC.db[tradeskillName], {["name"] = name, ["link"] = link})
        end
    end
    PrintMsg("WhoCanCraft: Updated craftable recipes for " .. tradeskillName)
    return true
end


function getCraftSkills()
    local name, type, link
    local craftName, _, _ = GetCraftDisplaySkillLine()
    if not(table.contains(professions, craftName)) then
        return false
    end
    WCC.db[craftName] = {}
    for i = 1, GetNumCrafts() do
        name, _, type, _, _, _, _ = GetCraftInfo(i)
        link = GetCraftItemLink(i)
        if (name and link and craftName and type ~= "header") then
            table.insert(WCC.db[craftName], {["name"] = name, ["link"] = link})
        end
    end
    PrintMsg("WhoCanCraft: Updated craftable recipes for " .. craftName)
    return true
end


string.startswith = function(self, str) 
    return string.find(str, '^' .. str) ~= nil
end


table.contains = function(self, val)
    for _, value in pairs(self) do
        if value == val then
            return true
        end
    end

    return false
end


CreateFrame("Frame", "WhoCanCraft");

WhoCanCraft:SetScript("OnEvent", function()
	if event == "ADDON_LOADED" then
        WhoCanCraft:UnregisterEvent("ADDON_LOADED");
        LoadVariables();
    elseif event == "TRADE_SKILL_SHOW" then
        getTradeSkills();
    elseif event == "CRAFT_SHOW" then
        getCraftSkills();
    elseif event == "CHAT_MSG_GUILD" then
        local message, player = string.lower(arg1), arg2

        if string.startswith(message, "wcc ") then
            query = string.gsub(message, "^(%s*wcc%s*)(.-)%s*$", "%2");
            canCraft = canICraft(query);
            if table.getn(canCraft) > 0 and player ~= UnitName("Player") then
                SendChatMessage("I can craft: " .. table.concat(canCraft, ", "), "WHISPER", nil, player);            
            end
        end
    end

end);

WhoCanCraft:RegisterEvent("ADDON_LOADED");
WhoCanCraft:RegisterEvent("TRADE_SKILL_SHOW");
WhoCanCraft:RegisterEvent("CRAFT_SHOW");
WhoCanCraft:RegisterEvent("CHAT_MSG_GUILD")