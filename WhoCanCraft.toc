## Interface: 11200
## Title: WhoCanCraft
## Notes: Filter through available recipes and let guildies know
## Author: Satarial
## Version: 0.0.1
## SavedVariablesPerCharacter: WCC
WhoCanCraft.lua